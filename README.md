# Readme для приложения на Spring Boot 2 с поддержкой Java 17, безопасностью, использованием Lombok, PostgreSQL и Docker для CRUD операций с пользователями, задачами и ролями.

## Описание

Это руководство по установке, настройке и использованию приложения на Spring Boot 2 с поддержкой Java 17, безопасностью,
использованием Lombok, PostgreSQL и Docker для CRUD операций с пользователями, задачами и ролями.

## Требования

Для запуска этого приложения вам понадобятся следующие требования:

- Java 17
- PostgreSQL
- Docker 

## Установка и настройка

### Шаг 1: Клонирование репозитория

Клонируйте репозиторий приложения на свою локальную машину:

```
git clone https://gitlab.com/VladSemenovForVibeLab/application-for-scientific-calculations.git
```

### Шаг 2: Настройка PostgreSQL

Установите PostgreSQL в своей системе, если он еще не установлен. Создайте базу данных и настройте соответствующие
параметры в файле `application.properties`:

```
spring.datasource.url=jdbc:postgresql://localhost:5432/<имя_базы_данных>
spring.datasource.username=<имя_пользователя>
spring.datasource.password=<пароль>
```

### Шаг 3: Запуск приложения с помощью Docker 

Если у вас есть Docker, вы можете запустить приложение в контейнере Docker. В каталоге проекта выполните следующие
команды:

```
docker-compose up
```

### Шаг 4: Запуск приложения без Docker

Если вы не планируете использовать Docker, вы можете запустить приложение локально. В каталоге проекта выполните
следующую команду:

```
./mvnw spring-boot:run
```

## Использование

После успешной установки и запуска приложения вы сможете выполнить CRUD операции с пользователями, задачами и ролями с
помощью REST API через удобный инструмент API, такой как `curl`, `Postman` или `Insomnia`.

## Дополнительная информация

- Весь исходный код приложения находится в репозитории GitLab: https://gitlab.com/VladSemenovForVibeLab/application-for-scientific-calculations.git
- Документация Spring Boot: https://spring.io/projects/spring-boot
- Документация Java 17: https://docs.oracle.com/en/java/javase/17/
- Документация PostgreSQL: https://www.postgresql.org/docs/

## Автор

Семенов В.В 
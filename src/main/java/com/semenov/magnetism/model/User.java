package com.semenov.magnetism.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "user_table")
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "username")
    @Size(min = 2, max = 100, message = "Username should be from 2 to 100 chars")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "firstName")
    @Size(min = 2, max = 100, message = "Firstname should be from 2 to 100 chars")
    private String firstName;
    @Size(min = 2, max = 100, message = "Lastname should be from 2 to 100 chars")
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "email")
    private String email;
    @Transient
    private List<String> authorities;
}

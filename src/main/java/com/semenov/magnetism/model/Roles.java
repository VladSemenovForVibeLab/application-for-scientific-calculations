package com.semenov.magnetism.model;

public enum Roles {
    ROLE_USER,
    ROLE_ADMIN
}

package com.semenov.magnetism.service.impl;

import com.semenov.magnetism.model.Role;
import com.semenov.magnetism.repository.RoleRepository;
import com.semenov.magnetism.service.interf.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl extends AbstractCRUDService<Role,Long> implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Override
    CrudRepository<Role, Long> getRepository() {
        return roleRepository;
    }
}

package com.semenov.magnetism.service.impl;

import com.semenov.magnetism.model.Task;
import com.semenov.magnetism.repository.TaskRepository;
import com.semenov.magnetism.service.interf.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service

public class TaskServiceImpl extends AbstractCRUDService<Task,Long> implements TaskService {

    @Autowired
    private TaskRepository taskRepository;
    @Override
    CrudRepository<Task, Long> getRepository() {
        return taskRepository;
    }
}

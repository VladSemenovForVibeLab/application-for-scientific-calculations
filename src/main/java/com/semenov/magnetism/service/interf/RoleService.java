package com.semenov.magnetism.service.interf;

import com.semenov.magnetism.model.Role;

public interface RoleService extends CRUDService<Role,Long>{

}

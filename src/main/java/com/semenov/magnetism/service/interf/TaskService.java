package com.semenov.magnetism.service.interf;

import com.semenov.magnetism.model.Task;

public interface TaskService extends CRUDService<Task,Long> {
}

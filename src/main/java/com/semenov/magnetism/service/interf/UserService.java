package com.semenov.magnetism.service.interf;

import com.semenov.magnetism.model.User;

public interface UserService extends CRUDService<User,Long> {
}

package com.semenov.magnetism.repository;

import com.semenov.magnetism.model.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole,Long> {
    Iterable<UserRole> findAllByUserId(Long userId);
}

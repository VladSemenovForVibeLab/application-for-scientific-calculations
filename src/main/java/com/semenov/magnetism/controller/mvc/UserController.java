package com.semenov.magnetism.controller.mvc;

import com.semenov.magnetism.model.Task;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(UserController.USER_URL)
public class UserController extends CRUDController<Task,Long> {
    public static final String USER_URL="ui/user";
    public static final String USER_NAME="user";

    @Override
    String getEntityName() {
        return USER_NAME;
    }
}
